<?php

/**
 * @file
 * Provides custom menu controller functionalities in Drupal.
 *
 * This file contains the MenuController class, which extends
 * MenuAdminPerMenuController to provide custom functionalities for the menu
 * overview page. It includes methods to construct and render the menus
 * overview page with specific access controls based on permissions.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu\Controller;

use Drupal\codev_menu\MenuManager;
use Drupal\menu_admin_per_menu\Controller\MenuAdminPerMenuController;

/**
 * Controller routines for the menu overview route.
 *
 * This class extends MenuAdminPerMenuController to provide custom controller
 * logic for the menu overview page. It includes a method to construct the
 * menus overview page with access control logic based on user permissions
 * defined in MenuManager.
 *
 * @noinspection PhpUnused
 */
class MenuController extends MenuAdminPerMenuController {

  /**
   * Constructs the menus overview page with custom access control.
   *
   * This method overrides the parent method to apply additional access control
   * logic for displaying menus in the overview page. It filters the menus
   * based on user permissions, showing only those menus the user has
   * permission to access.
   *
   * @return array
   *   The renderable array for the menus overview page, with menus filtered
   *   based on permissions.
   */
  public function menuOverviewPage(): array {
    $account = $this->currentUser();
    $menu_table = parent::menuOverviewPage();

    if (!empty($menu_table['table']['#rows'])
      && $account->hasPermission(MenuManager::MENU_PERM_KEY)
      && !$account->hasPermission(MenuManager::MENU_ADMIN_PERM_KEY)) {
      foreach ($menu_table['table']['#rows'] as $id => $row) {
        $perm_key = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $id);
        if (!$account->hasPermission($perm_key)) {
          unset($menu_table['table']['#rows'][$id]);
        }
      }
    }

    return $menu_table;
  }

}
