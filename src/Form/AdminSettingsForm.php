<?php

/**
 * @file
 * Provides a form for configuring menu settings in Drupal.
 *
 * This file contains the AdminSettingsForm class, which extends ConfigFormBase
 * to provide a customizable configuration form for menu settings in Drupal.
 * It includes methods for building the form, validating and submitting form
 * data, and handling AJAX interactions.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu\Form;

use Drupal;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Entity\Menu;
use Exception;

/**
 * Form class for configuring menu settings.
 *
 * This class provides a configuration form for managing various menu settings.
 * It includes functionalities for creating visibility categories, sorting
 * settings, and handling AJAX requests for adding and removing items from the
 * form.
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codev_menu_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var Config $config */
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    // Create visibility categories edit table.
    $form['categories'] = $this->buildCategoryElements($data, $form_state);

    // Create sorting settings elements.
    $form['sorting_settings'] = $this->buildSortingSettingsElements($data);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_menu.settings',
    ];
  }

  /**
   * Create visibility categories edit table.
   *
   * @param array              $data
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  private function buildCategoryElements(array $data, FormStateInterface $form_state): array {
    $categories_count = 0;

    // Create visibility categories edit table.
    $ret = [
      '#type'   => 'fieldset',
      '#title'  => $this->t('Menu link visibility categories'),
      '#group'  => 'sidebar',
      '#weight' => 0,
      '#prefix' => '<div id="categories-wrapper">',
      '#suffix' => '</div>',
    ];

    if ($form_state->isRebuilding()) {
      $ret['unsaved'] = [
        '#markup'     => sprintf(
          '<div class="messages messages--warning">%s</div>',
          $this->t('You have unsaved changes.')),
        '#attributes' => [
          'class' => [
            'view-changed',
            'messages',
            'messages--warning',
          ],
        ],
        '#weight'     => -9999,
      ];
    }

    $ret['categories_table'] = [
      '#type'   => 'table',
      '#header' => [
        '',
        $this->t('Label'),
        $this->t('Machine name'),
      ],
      '#empty'  => $this->t('No categories found!'),
    ];

    if (!empty($data['categories'])) {
      $categories = $data['categories'];
      foreach ($categories as $key => $value) {
        $label = Utils::getArrayValue('label', $value, '');
        $ret['categories_table'][$categories_count] = $this->buildCategoryTableItem($label, $key);
        $categories_count++;
      }
    }

    $categories_id = $form_state->get('categories_id');
    if ($categories_id === NULL) {
      $items = empty($categories_count) ? 1 : 0;
      $form_state->set('categories_id', $items);
      $categories_id = $items;
    }

    for ($i = 0; $i < $categories_id; $i++) {
      $ret['categories_table'][$categories_count] = $this->buildCategoryTableItem();
      $categories_count++;
    }

    foreach ($form_state->get('unset_ids') ?: [] as $id) {
      if (!empty($ret['categories_table'][$id])) {
        unset($ret['categories_table'][$id]);
      }
    }

    $ret['add'] = [
      '#type'   => 'submit',
      '#value'  => $this->t('Add new'),
      '#submit' => ['::addNewItemToTable'],
      '#ajax'   => [
        'callback' => '::updateCategoryTableElement',
        'wrapper'  => 'categories-wrapper',
      ],
    ];

    $ret['remove'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Delete'),
      '#submit'     => ['::deleteItemFromTable'],
      '#ajax'       => [
        'callback' => '::updateCategoryTableElement',
        'wrapper'  => 'categories-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
    ];

    return $ret;
  }

  /**
   * Return the category table item.
   *
   * @param string $label
   * @param string $id
   *
   * @return array
   */
  private function buildCategoryTableItem(string $label = '', string $id = ''): array {
    return [
      'delete' => [
        '#type' => 'checkbox',
      ],
      'label'  => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Label'),
        '#title_display' => 'invisible',
        '#default_value' => $label,
        '#required'      => TRUE,
      ],
      'id'     => [
        '#type'          => 'textfield',
        '#title'         => $this->t('ID'),
        '#title_display' => 'invisible',
        '#required'      => TRUE,
        '#default_value' => $id,
        '#disabled'      => !empty($id),
      ],
    ];
  }

  /**
   * Create visibility categories edit table.
   *
   * @param array $data
   *
   * @return array
   */
  private function buildSortingSettingsElements(array $data): array {
    $ret = [
      '#type'   => 'details',
      '#title'  => $this->t('Sorting Settings'),
      '#group'  => 'sidebar',
      '#weight' => 0,
      '#open'   => !$data['global_sorting'],
    ];

    $ret['global_sorting'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Activate or deactivate the global menu link sorting for mobile menu blocks.'),
      '#default_value' => Utils::getArrayValue('global_sorting', $data, ''),
    ];

    $ret['menus_table'] = [
      '#type'        => 'fieldgroup',
      '#title'       => $this->t('Menu sorting:'),
      '#description' => t('Here you can adjust the sorting of the menus.'),
      '#states'      => [
        'visible' => [
          ':input[name="global_sorting"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $ret['menus_table']['menu_sorting'] = [
      '#type'      => 'table',
      '#header'    => [
        $this->t('Title'),
        $this->t('Description'),
        $this->t('Weight'),
      ],
      '#empty'     => $this->t('No menus found!'),
      '#tabledrag' => [
        [
          'action'       => 'order',
          'relationship' => 'sibling',
          'group'        => 'row-weight',
        ],
      ],
    ];

    $menu_table_items = [];
    $menus = $this->loadAllMenus();
    foreach ($menus as $menu) {
      /** @var Menu $menu */
      $id = $menu->id();
      $weight = $data['menu_sorting'][$id]['weight'] ?? 0;
      $menu_table_items[$id]['#weight'] = $weight;
      $menu_table_items[$id]['#attributes'] = [
        'class' => ['draggable'],
      ];

      $menu_table_items[$id]['title'] = [
        '#markup' => $menu->label(),
      ];

      $menu_table_items[$id]['description'] = [
        '#markup' => $menu->getDescription(),
      ];

      $menu_table_items[$id]['weight'] = [
        '#type'          => 'weight',
        '#title'         => t('Weight for @title', [
          '@title' => $menu->label(),
        ]),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes'    => [
          'class' => [
            'row-weight',
          ],
        ],
      ];
    }
    Utils::sortArrayByKey($menu_table_items, '#weight');
    $ret['menus_table']['menu_sorting'] += $menu_table_items;

    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $uniq_array = [];
    $duplicates_array = [];
    $triggering_element = $form_state->getTriggeringElement();

    foreach ($form_state->getValue('categories_table') ?: [] as $key => $item) {
      $id = Utils::getArrayValue('id', $item, '');
      if (!in_array($id, $uniq_array)) {
        $uniq_array[] = $id;
      }
      else {
        $duplicates_array[] = [
          'id'  => $id,
          'key' => $key,
        ];
      }
    }

    foreach ($duplicates_array as $val) {
      if (!empty($val['key']) && !empty($form['categories']['categories_table'][$val['key']])) {
        $form_state->setError($form['categories']['categories_table'][$val['key']]['id'],
          $this->t('An entry with the ID: "@id" exists already.', ['@id' => $val['id']]));
      }
    }

    if (!empty($triggering_element['#submit'])
      && in_array('::deleteItemFromTable', $triggering_element['#submit'])) {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->getEditableConfigNames()[0]);

    // Prepare tables infos.
    $categories = [];
    foreach ($form_state->getValue('categories_table') as $item) {
      $label = Utils::getArrayValue('label', $item, '');
      $categories[$item['id']] = [
        'label' => $label,
      ];
    }

    // Save to config.
    $config->set('categories', $categories)
      ->set('global_sorting', $form_state->getValue('global_sorting'))
      ->set('menu_sorting', $form_state->getValue('menu_sorting'))
      ->save();
  }

  /**
   * Add item to table by ajax.
   *
   * @param array              $form
   *   Form array.
   * @param FormStateInterface $form_state
   *   Form state object.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewItemToTable(array &$form, FormStateInterface $form_state): void {
    $categories_id = $form_state->get('categories_id');
    $form_state->set('categories_id', $categories_id + 1);
    $form_state->setRebuild();
  }

  /**
   * Update callback for ajax.
   *
   * @param array              $form
   *   Form array.
   * @param FormStateInterface $form_state
   *   Form state object.
   *
   * @return mixed
   *   Return categories ajax item info.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function updateCategoryTableElement(array $form, FormStateInterface $form_state): mixed {
    return $form['categories'];
  }

  /**
   * Delete item from table by ajax.
   *
   * @param array              $form
   *   Form array.
   * @param FormStateInterface $form_state
   *   Form state object.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteItemFromTable(array &$form, FormStateInterface $form_state): void {
    $items = $form_state->get('unset_ids') ?: [];
    foreach ($form_state->getValue('categories_table') ?: [] as $key => $item) {
      if ($item['delete']) {
        $items[] = $key;
      }
    }
    $form_state->set('unset_ids', $items);
    $form_state->setRebuild();
  }

  /**
   * Load all menus.
   *
   * @return array
   */
  private function loadAllMenus(): array {
    try {
      return Drupal::entityTypeManager()
        ->getStorage('menu')
        ->loadMultiple();
    } catch (Exception) {
      return [];
    }
  }

}
