<?php

/**
 * @file
 * Provides access control and management functions for menu-related operations
 * in Drupal.
 *
 * This file contains the MenuManager class, which provides a series of methods
 * for managing menus, including checking permissions, loading menus based on
 * user permissions, and handling menu options. It serves as a utility class
 * for the codev_menu module, enhancing menu management and access control.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu;

use Drupal;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\TypedData\Exception\ReadOnlyException;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\NodeType;
use Drupal\system\Entity\Menu;
use Exception;

/**
 * Helper class to manage menu items and trees.
 *
 * This class provides utility methods for managing Drupal menu items, trees,
 * and permissions. It includes functions for loading menus, saving and
 * appending menu options, and checking user permissions for specific menu
 * operations.
 */
class MenuManager {

  /**
   * Menu permission key string.
   */
  public const MENU_PERM_KEY = 'administer menu';

  /**
   * Extended menu permission key string.
   */
  public const MENU_ADMIN_PERM_KEY = 'administer menu admin';

  /**
   * Menu permission key string, for sprintf.
   */
  public const MENU_ITEMS_PERM_KEY = 'administer %s menu items';

  /**
   * Check if the user has the permission to use the extended menu add
   * functions.
   *
   * @param string                $mid
   *   Menu key id.
   * @param AccountInterface|null $account
   *   User account object or null, if null the current user is determined
   *   automatically.
   *
   * @return bool
   *   TRUE if the user has permission, otherwise FALSE.
   */
  public static function hasPermissions(string $mid, ?AccountInterface $account = NULL): bool {
    if (!$account instanceof AccountInterface) {
      $account = Drupal::currentUser();
    }
    if ($account->hasPermission(static::MENU_ADMIN_PERM_KEY)
      || $account->hasPermission(sprintf(static::MENU_ITEMS_PERM_KEY, $mid))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Load all menus by checking on the user permissions.
   *
   * @param AccountInterface|null $account
   *   User account object or null, if null the current user is determined
   *   automatically.
   *
   * @return array
   *   Return all menus as an array.
   */
  public static function loadMultipleByUser(?AccountInterface $account = NULL): array {
    try {
      $menus = Drupal::entityTypeManager()
        ->getStorage('menu')
        ->loadmultiple();
      foreach ($menus as $menu) {
        if (!static::hasPermissions($menu->id(), $account)) {
          unset($menus[$menu->id()]);
        }
      }
      return $menus;
    } catch (Exception) {
      return [];
    }
  }

  /**
   * Get all menu id as option array.
   *
   * @return array
   *   Return all menu id's from database.
   */
  public static function getSelectList(): array {
    $ret = [];
    /** @var Menu[] $menus */
    $menus = static::loadMultipleByUser();
    foreach ($menus as $menu) {
      $ret[$menu->id()] = $menu->label();
    }
    return $ret;
  }

  /**
   * Get menu options from menu tree table.
   *
   * @param string $mid
   *   Menu id.
   *
   * @return array
   *   Menu tree items array.
   * @throws Exception
   */
  public static function getMenuItemOptionsFromTree(string $mid): array {
    $connection = Drupal::database();
    $update = $connection->select('menu_tree', 'm');
    $update->fields('m', ['options']);
    $update->condition('id', $mid);
    $result = $update->execute()->fetchAll();
    return unserialize($result[0]->options ?? '') ?: [];
  }

  /**
   * Save menu options to menu tree table.
   *
   * @param string $mid
   *   Menu item id.
   * @param array  $options
   *   Options array to save to the menu tree item.
   *
   * @throws Exception
   */
  public static function saveMenuItemOptionsToTree(string $mid, array $options): void {
    $connection = Drupal::database();
    $update = $connection->update('menu_tree');
    $update->fields(['options' => serialize($options)])
      ->condition('id', $mid)->execute();
  }

  /**
   * Append menu options to menu tree table.
   *
   * @param string $mid
   *   Menu item id.
   * @param array  $options
   *   Options array to save to the menu tree item.
   *
   * @throws Exception
   */
  public static function appendMenuItemOptionsToTree(string $mid, array $options): void {
    $menu_link_options = static::getMenuItemOptionsFromTree($mid);
    $merged = array_merge($menu_link_options, $options);
    static::saveMenuItemOptionsToTree($mid, $merged);
  }

  /**
   * Save menu options to menu Link Content.
   *
   * @param MenuLinkContent $menu_link
   *   Menu item id.
   * @param array           $options
   *   Options array to save to the menu tree item.
   *
   * @throws EntityStorageException
   * @throws MissingDataException
   * @throws ReadOnlyException
   */
  public static function saveMenuItemOptionsToLinkContent(MenuLinkContent $menu_link, array $options): void {
    $menu_link_item = $menu_link->get('link')->first();
    $menu_link_item_val = $menu_link_item->getValue();
    $menu_link_item_val['options'] = $options;
    $menu_link_item->setValue($menu_link_item_val);
    $menu_link->save();
  }

  /**
   * Append menu options to menu Link Content.
   *
   * @param MenuLinkContent $menu_link
   *   Menu item id.
   * @param array           $options
   *   Options array to save to the menu tree item.
   *
   * @throws EntityStorageException
   * @throws MissingDataException
   * @throws ReadOnlyException
   */
  public static function appendMenuItemOptionsToLinkContent(MenuLinkContent $menu_link, array $options): void {
    $menu_link_item = $menu_link->get('link')->first();
    $menu_link_item_val = $menu_link_item->getValue();
    $options = array_merge($menu_link_item_val['options'] ?? [], $options);
    static::saveMenuItemOptionsToLinkContent($menu_link, $options);
  }

  /**
   * Get all nodes types by menu id.
   *
   * @param string $mid
   *
   * @return array
   */
  public static function getNodeTypes(string $mid): array {
    $ret = [];
    try {
      /** @var NodeType[] $node_types */
      $node_types = Drupal::entityTypeManager()
        ->getStorage('node_type')
        ->loadMultiple();
      foreach ($node_types as $id => $type) {
        $menus = $type->getThirdPartySetting('menu_ui', 'available_menus') ?: [];
        if (in_array($mid, $menus)) {
          $ret[] = $id;
        }
      }
    } catch (Exception) {
      return [];
    }
    return $ret;
  }

}
