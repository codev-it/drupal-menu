<?php

/**
 * @file
 * Provides a class for managing settings related to the codev_menu module in
 * Drupal.
 *
 * This file contains the Settings class, which extends the SettingsBase class.
 * It is responsible for managing various settings specific to the codev_menu
 * module, such as visibility categories, link targets, and node types. It
 * provides methods to retrieve these settings as option arrays for use in
 * forms and other components.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu;

use Drupal;
use Drupal\codev_utils\SettingsBase;
use Exception;

/**
 * Class for managing settings of the codev_menu module.
 *
 * This class provides methods for retrieving and managing settings related to
 * menu visibility categories, link targets, and node types. It allows easy
 * access and manipulation of these settings, which are used throughout the
 * codev_menu module.
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * The module name for the settings.
   */
  public const MODULE_NAME = 'codev_menu';

  /**
   * Get visibility categories from configuration as an option array.
   *
   * Retrieves visibility categories defined in the module's configuration and
   * returns them as an array suitable for select list options.
   *
   * @return array
   *   An array of visibility category items.
   */
  public static function getCategoriesAsList(): array {
    return array_map(function ($val) {
      return t($val['label']);
    }, static::get('categories') ?: []);
  }

  /**
   * Get target config as option array.
   *
   * @return array
   *   Link target option array.
   */
  public static function getTargetsAsList(): array {
    return array_map(function ($val) {
      return t($val['label']);
    }, static::get('target') ?: []);
  }

  /**
   * Return a list of node types.
   *
   * @return array
   */
  public static function getNodeList(): array {
    $ret = [];
    try {
      $node_types = Drupal::entityTypeManager()
        ->getStorage('node_type')
        ->loadMultiple();
      foreach ($node_types as $id => $type) {
        $ret[$id] = $type->label();
      }
    } catch (Exception) {
      return [];
    }
    return $ret;
  }

}
