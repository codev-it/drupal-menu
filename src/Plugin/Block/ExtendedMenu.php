<?php

/**
 * @file
 * Provides an advanced menu block with categorization and combination options
 * in Drupal.
 *
 * This file contains the ExtendedMenu class, which extends BlockBase and
 * implements ContainerFactoryPluginInterface. It provides a block plugin for
 * displaying an advanced menu with options for categorization and combination
 * of menu items.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu\Plugin\Block;

use Drupal\codev_menu\MenuManager;
use Drupal\codev_menu\Settings;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\InaccessibleMenuLink;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an advanced menu block with categorization and combination options.
 *
 * This block plugin allows for the creation of advanced menus with features
 * such as categorization and the combination of different menus into a single
 * block. It includes custom block form settings and logic for building and
 * rendering the combined menu.
 *
 * @Block(
 *   id = "extended_menu",
 *   admin_label = @Translation("Extended menu"),
 *   category = @Translation("Menus")
 * )
 *
 * @noinspection PhpUnused
 */
class ExtendedMenu extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu tree.
   *
   * @var MenuLinkTreeInterface
   */
  protected MenuLinkTreeInterface $menuTree;

  /**
   * The database connection.
   *
   * @var Connection
   */
  protected Connection $database;

  /**
   * {@inheritdoc}
   * @noinspection PhpFieldAssignmentTypeMismatchInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ContainerFactoryPluginInterface|ExtendedMenu|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->menuTree = $container->get('menu.link_tree');
    $instance->database = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // Include custom display settings.
    $categories = Settings::getCategoriesAsList();
    if (!empty($categories)) {
      $categories_default = Utils::getArrayValue('categories', $config, []);
      $form['categories'] = [
        '#type'          => 'checkboxes',
        '#title'         => $this->t('Menu visibility categories'),
        '#description'   => $this->t('Sets the visibility of the menu link based on the menu categories.'),
        '#options'       => $categories,
        '#default_value' => $categories_default,
        '#required'      => TRUE,
      ];
    }

    // Include custom display settings.
    $categories_options = MenuManager::getSelectList();
    if (!empty($categories_options)) {
      $categories_default = Utils::getArrayValue('menus', $config, []);
      $form['menus'] = [
        '#type'          => 'checkboxes',
        '#title'         => $this->t('Select the menus to be displayed:'),
        '#description'   => $this->t('All selected menus are displayed in this block.'),
        '#options'       => $categories_options,
        '#default_value' => $categories_default,
        '#required'      => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['menus'] = $this->cleanArrayValues($values['menus'] ?: []);
    $this->configuration['categories'] = $this->cleanArrayValues($values['categories'] ?: []);
  }

  /**
   * Build clean array from values.
   *
   * @param array $arr
   *   Array to clean.
   *
   * @return array
   *   Cleaned array.
   */
  private function cleanArrayValues(array $arr): array {
    $ret = [];
    foreach ($arr as $item) {
      if (!empty($item)) {
        $ret[] = $item;
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   *
   * @throws Exception
   */
  public function build(): array {
    $items = $this->loadCombinedMenuItems();
    $cache_tags = [];
    foreach ($this->getMenuIds() as $menu_name) {
      $cache_tags[] = 'config:system.menu.' . $menu_name;
    }
    return [
      '#menu_name' => $this->getPluginId(),
      '#theme'     => 'menu',
      '#items'     => $items,
      '#cache'     => [
        'tags' => $cache_tags,
      ],
    ];
  }

  /**
   * Loads mobile menu items.
   *
   * @return array
   *   The loaded mobile menu items.
   *
   * @throws Exception
   */
  protected function loadCombinedMenuItems(): array {
    $menus_to_merge = $this->getMenuIds();

    if (empty($menus_to_merge)) {
      return [];
    }

    $menu_tree = $this->mergeMenus($menus_to_merge);
    $menu = $this->menuTree->build($menu_tree);
    $items = $menu['#items'] ?? [];
    return $this->checkLinkVisibility($items);
  }

  /**
   * Returns a list of menu IDs that should be included in the mobile menu.
   *
   * @return string[]
   *   A list of menu IDs that should be included in the mobile menu.
   */
  protected function getMenuIds(): array {
    return $this->configuration['menus'] ?? [];
  }

  /**
   * Builds a merged menu tree for the given menu names.
   *
   * @param array $menu_names
   *   The menu names.
   *
   * @return MenuLinkTreeElement[]
   *   A merged menu tree for the given menu names.
   *
   * @throws Exception
   */
  protected function mergeMenus(array $menu_names = []): array {
    if (empty($menu_names)) {
      return [];
    }

    $item_array = [];
    $menu_base_weight = 0;
    $use_global_sorting = Settings::get('global_sorting');
    $menu_sorting = Settings::get('menu_sorting') ?: [];
    foreach ($menu_names as $menu_name) {
      if (empty($menu_name)) {
        continue;
      }

      if (!$use_global_sorting) {
        $menu_base_weight = $this->getMenuWeight($menu_name, $menu_sorting);
      }
      $tree = $this->getMenuItemsTree($menu_name);
      foreach ($tree as $item) {
        $link = $item->link;

        if (!$link->isEnabled() && $link instanceof InaccessibleMenuLink) {
          continue;
        }

        if ($menu_base_weight) {
          $original_weight = $link->getWeight();
          $link->updateLink([
            'weight' => $menu_base_weight + $original_weight,
          ], FALSE);
        }
        $item_array[] = $item;
      }
    }
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    return $this->menuTree->transform($item_array, $manipulators);
  }

  /**
   * Get menu Weight.
   *
   * @param string $menu
   *   Menu name.
   * @param array  $arr
   *   Menu items to search for.
   *
   * @return int|null
   *   Return the Weight.
   */
  protected function getMenuWeight(string $menu, array $arr): ?int {
    foreach ($arr as $key => $item) {
      if ($key == $menu) {
        return intval($item['weight'] . '000');
      }
    }
    return NULL;
  }

  /**
   * Builds a menu link tree for the given menu (name).
   *
   * @param string $menu_name
   *   The menu names.
   *
   * @return MenuLinkTreeElement[]
   *   The built menu link tree for the given menu name.
   *
   * @throws Exception
   */
  protected function getMenuItemsTree(string $menu_name): array {
    if (empty($menu_name)) {
      return [];
    }
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name);
    $parentsToExpand = $this->getAllParents($menu_name);
    if (!empty($parentsToExpand)) {
      $parameters->addExpandedParents($parentsToExpand);
    }
    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = [['callable' => 'menu.default_tree_manipulators:checkAccess']];
    return $this->menuTree->transform($tree, $manipulators);
  }

  /**
   * Finds all links in a menu given a set of possible parents having children.
   *
   * In contrary to MenuTreeStorage::getExpanded, the 'expanded' property is not
   * a criterion.
   *
   * @param string $menu_name
   *   Menu name.
   *
   * @return array
   *   Full menu parent tree.
   *
   * @throws Exception
   *
   * @see \Drupal\Core\Menu\MenuTreeStorage::getExpanded
   */
  protected function getAllParents(string $menu_name): array {
    $parents = [];
    $table = 'menu_tree';
    do {
      $query = $this->database->select($table, []);
      $query->fields($table, ['id']);
      $query->condition('menu_name', $menu_name);
      $query->condition('has_children', 1);
      $query->condition('enabled', 1);
      if (empty($parents)) {
        $query->condition('depth', 1);
      }
      else {
        $query->condition('parent', $parents, 'IN');
        $query->condition('id', $parents, 'NOT IN');
      }
      $result = $query->execute()->fetchAllKeyed(0, 0);
      $parents += $result;
    } while (!empty($result));
    return $parents;
  }

  /**
   * Check menu items recursively if there are excluded from the block.
   *
   * @param array $arr
   *   Array to check for excluded items.
   *
   * @return array
   *   Checked and formatted array.
   */
  protected function checkLinkVisibility(array &$arr): array {
    foreach ($arr as $key => &$item) {
      if (!empty($item['below'])) {
        $item['below'] = $this->checkLinkVisibility($item['below']);
      }
      if ($this->isItemExcluded($item)) {
        unset($arr[$key]);
      }
    }
    return $arr;
  }

  /**
   * Check if is item excluded form block category.
   *
   * @param array $item
   *   Item to check for exclusion.
   *
   * @return bool
   *   Return true if item excluded.
   */
  protected function isItemExcluded(array $item): bool {
    /** @var Url $url */
    $url = $item['url'];
    $block_cats = array_filter($this->configuration['categories'], 'is_string');
    $hide_on = array_filter($url->getOption('codev_menu')['hide_on'] ?? [], 'is_string');
    foreach ($block_cats as $cat) {
      if ($cat != '0' && in_array($cat, $hide_on)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
