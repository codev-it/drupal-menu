<?php

/**
 * @file
 * Provides functionality for managing roles and permissions related to menu
 * items in Drupal.
 *
 * This file contains the RoleManager class, which is responsible for managing
 * user roles and their permissions in relation to menu items. It includes
 * methods for retrieving user roles, granting and revoking permissions for
 * menu items, and managing role-based access to menus.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu;

use Drupal;
use Drupal\codev_utils\Helper\User;
use Drupal\user\Entity\Role;
use Exception;

/**
 * Helper class to manage roles and permissions for menu items.
 *
 * This class provides utility methods for managing roles and their permissions
 * in the context of menu items. It includes functions for getting user roles,
 * setting permissions for menu items, and retrieving roles with specific menu
 * permissions.
 */
class RoleManager {

  /**
   * Get current user roles as an options list.
   *
   * Retrieves the roles of the current user and returns them as an array
   * suitable for select list options.
   *
   * @return array
   *   An array of user roles.
   */
  public static function getCurrentUserSelectList(): array {
    $ret = [];
    $roles = User::getAllRoles(FALSE, TRUE);
    /** @var Role $role */
    foreach ($roles as $role) {
      $ret[$role->id()] = $role->label();
    }
    return $ret;
  }

  /**
   * Get all roles with permissions for a given menu ID.
   *
   * @param string $mid
   *   Menu ID.
   *
   * @return array
   *   An array of roles with permissions for the menu.
   */
  public static function getMenuItemPermitted(string $mid): array {
    try {
      $ret = [];
      $roles = Drupal::entityTypeManager()
        ->getStorage('user_role')
        ->loadMultiple();
      /** @var Role $role */
      foreach ($roles as $role) {
        $permission = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $mid);
        if ($role->hasPermission($permission)) {
          $ret[] = $role->id();
        }
      }
      return $ret;
    } catch (Exception) {
      return [];
    }
  }

  /**
   * Set add/edit permission for a menu by given role.
   *
   * @param string $mid
   *   Menu ID.
   * @param string $role_id
   *   User role ID.
   *
   * @return bool|int
   *   TRUE if the permission was successfully set, otherwise FALSE.
   */
  public static function addMenuItemPermission(string $mid, string $role_id): bool|int {
    try {
      /** @var Role $role */
      $role = Drupal::entityTypeManager()
        ->getStorage('user_role')
        ->load($role_id);
      $perm_key = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $mid);
      if ($role && !$role->hasPermission($perm_key)) {
        $role->grantPermission($perm_key);
        return $role->save();
      }
      return FALSE;
    } catch (Exception) {
      return FALSE;
    }
  }

  /**
   * Remove add/edit permission for a menu by given role.
   *
   * @param string $mid
   *   Menu ID.
   * @param string $role_id
   *   User role ID.
   *
   * @return bool|int
   *   TRUE if the permission was successfully removed, otherwise FALSE.
   */
  public static function removeMenuItemPermission(string $mid, string $role_id): bool|int {
    try {
      /** @var Role $role */
      $role = Drupal::entityTypeManager()
        ->getStorage('user_role')
        ->load($role_id);
      $perm_key = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $mid);
      if ($role && $role->hasPermission($perm_key)) {
        $role->revokePermission($perm_key);
        return $role->save();
      }
      return FALSE;
    } catch (Exception) {
      return FALSE;
    }
  }

}
