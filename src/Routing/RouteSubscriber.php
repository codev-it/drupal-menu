<?php

/**
 * @file
 * Listens to the dynamic route events for custom menu routing in Drupal.
 *
 * This file contains the RouteSubscriber class, which extends
 * RouteSubscriberBase. The class is responsible for listening to dynamic route
 * events and altering route configurations for menu-related routes in Drupal.
 * It customizes route behaviors and access control based on specific
 * requirements for the codev_menu module.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to and alters dynamic route events for the codev_menu module.
 *
 * This class listens to the dynamic routing events in Drupal and alters the
 * routes for menu-related operations. It includes custom access control and
 * route settings for menu entities and links, enhancing the functionality
 * provided by the module.
 *
 * @noinspection PhpUnused
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Run after content_translation, which has priority -210.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -230];
    return $events;
  }

  /**
   * Alters routes for menu-related entities and operations.
   *
   * This method customizes the routes for various menu-related operations such
   * as menu collection, edit forms, and link management. It sets custom access
   * control and controllers to manage these routes as per the requirements of
   * the codev_menu module.
   *
   * @param RouteCollection $collection
   *   The collection of routes to be altered.
   */
  protected function alterRoutes(RouteCollection $collection): void {
    $routes = $collection->all();
    foreach ($routes as $route_name => $route) {
      switch ($route_name) {
        case 'entity.menu.collection':
          $route->setDefaults([
            '_title'      => $route->getDefault('_title'),
            '_controller' => '\Drupal\codev_menu\Controller\MenuController::menuOverviewPage',
          ]);
          break;

        case 'entity.menu.edit_form':
        case 'entity.menu.add_link_form':
          $route->setRequirements(['_custom_access' => '\Drupal\codev_menu\Access\MenuAccess::menuAccess']);
          break;

        case 'menu_ui.link_edit':
        case 'menu_ui.link_reset':
          $route->setRequirements(['_custom_access' => '\Drupal\codev_menu\Access\MenuAccess::menuLinkAccess']);
          break;

        case 'entity.menu_link_content.canonical':
        case 'entity.menu_link_content.delete_form':
        case 'entity.menu_link_content.content_translation_overview':
        case 'entity.menu_link_content.content_translation_add':
        case 'entity.menu_link_content.content_translation_edit':
        case 'entity.menu_link_content.content_translation_delete':
          $route->setRequirements(['_custom_access' => '\Drupal\codev_menu\Access\MenuAccess::menuItemAccess']);
          break;
      }
    }
  }

}
