<?php

/**
 * @file
 * Provides access control for menu-related operations in Drupal.
 *
 * This file contains the MenuAccess class, which extends
 * MenuAdminPerMenuAccess
 * to provide custom access control for various menu-related operations. It
 * includes methods to check access permissions for menus, menu items, and menu
 * links.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_menu\Access;

use Drupal\codev_menu\MenuManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\menu_admin_per_menu\Access\MenuAdminPerMenuAccess;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;

/**
 * Provides access control handlers for menu-related operations.
 *
 * This class extends the MenuAdminPerMenuAccess class to offer custom access
 * control logic for menus, menu items, and menu link plugins. It utilizes the
 * MenuManager to define specific permissions and check user access based on
 * these permissions.
 *
 * @noinspection PhpUnused
 */
class MenuAccess extends MenuAdminPerMenuAccess {

  /**
   * Checks access for a menu.
   *
   * Determines whether the user has access to the given menu based on custom
   * permissions.
   *
   * @param AccountInterface $account
   *   The user account for access check.
   * @param Menu             $menu
   *   The menu entity to check access for.
   *
   * @return AccessResultNeutral|AccessResult|AccessResultAllowed
   *   The access result object.
   */
  public function menuAccess(AccountInterface $account, Menu $menu): AccessResultNeutral|AccessResult|AccessResultAllowed {
    $permission = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $menu->get('id'));
    $permissions = $this::getPerMenuPermissions($account);
    if ($account->hasPermission(MenuManager::MENU_ADMIN_PERM_KEY)
      || isset($permissions[$permission])) {
      return AccessResult::allowed();
    }
    return AccessResult::neutral();
  }

  /**
   * Checks access for a menu item.
   *
   * Determines whether the user has access to a specific menu item based on
   * custom permissions.
   *
   * @param AccountInterface     $account
   *   The user account for access check.
   * @param MenuLinkContent|null $menu_link_content
   *   The menu link content entity, if available.
   *
   * @return AccessResultNeutral|AccessResult|AccessResultAllowed
   *   The access result object.
   */
  public function menuItemAccess(AccountInterface $account, MenuLinkContent $menu_link_content = NULL): AccessResultNeutral|AccessResult|AccessResultAllowed {
    if (!$menu_link_content instanceof MenuLinkContent) {
      return AccessResult::neutral();
    }
    $permission = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $menu_link_content->getMenuName());
    $permissions = $this::getPerMenuPermissions($account);
    if ($account->hasPermission(MenuManager::MENU_ADMIN_PERM_KEY)
      || isset($permissions[$permission])) {
      return AccessResult::allowed();
    }
    return AccessResult::neutral();
  }

  /**
   * Checks access for a menu link plugin.
   *
   * Determines whether the user has access to the menu link plugin based on
   * custom permissions.
   *
   * @param AccountInterface       $account
   *   The user account for access check.
   * @param MenuLinkInterface|null $menu_link_plugin
   *   The menu link plugin instance, if available.
   *
   * @return AccessResultNeutral|AccessResult|AccessResultAllowed
   *   The access result object.
   */
  public function menuLinkAccess(AccountInterface $account, MenuLinkInterface $menu_link_plugin = NULL): AccessResultNeutral|AccessResult|AccessResultAllowed {
    if (!$menu_link_plugin instanceof MenuLinkInterface) {
      return AccessResult::neutral();
    }
    $permission = sprintf(MenuManager::MENU_ITEMS_PERM_KEY, $menu_link_plugin->getMenuName());
    $permissions = $this::getPerMenuPermissions($account);
    if ($account->hasPermission(MenuManager::MENU_ADMIN_PERM_KEY)
      || isset($permissions[$permission])) {
      return AccessResult::allowed();
    }
    return AccessResult::neutral();
  }

}
