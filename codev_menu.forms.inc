<?php

/**
 * @file
 * Contains hooks and helper functions to alter Drupal menus in the Codev-IT
 * project.
 *
 * This file provides a set of hooks and helper functions to modify Drupal menu
 * forms, add custom fields, and handle form submissions. These functions are
 * part of the menu management functionality in the Drupal Codev-IT project.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

use Drupal\codev_menu\MenuManager;
use Drupal\codev_menu\RoleManager;
use Drupal\codev_menu\Settings;
use Drupal\codev_utils\Helper\User;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\TypedData\Exception\ReadOnlyException;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\menu_ui\MenuForm;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeForm;
use Drupal\user\Entity\Role;


/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_edit_form.
 *
 * Alters the menu add form.
 *
 * Adds custom default settings to the menu add form. This function is used to
 * modify the form elements and layout of the Drupal menu add form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_menu_form_menu_add_form_alter(array &$form, FormStateInterface $form_state, $form_id): void {
  _codev_menu_form_menu_defaults($form, $form_state);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_edit_form.
 *
 * Alters the menu edit form.
 *
 * Adds custom default settings to the menu edit form. This function is used to
 * modify the form elements and layout of the Drupal menu edit form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_menu_form_menu_edit_form_alter(array &$form, FormStateInterface $form_state, $form_id): void {
  _codev_menu_form_menu_defaults($form, $form_state);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_link_edit.
 *
 * @noinspection PhpUnused
 */
function codev_menu_form_menu_link_edit_alter(array &$form, FormStateInterface $form_state): void {
  _codev_menu_form_menu_link_field_builder($form, $form_state);
  $form['#submit'][] = '_codev_menu_form_menu_link_submit';
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for menu_link_content_form.
 *
 * @noinspection PhpUnused
 */
function codev_menu_form_menu_link_content_form_alter(array &$form, FormStateInterface $form_state): void {
  _codev_menu_form_menu_link_field_builder($form, $form_state);
  $form['actions']['submit']['#submit'][] = '_codev_menu_form_menu_link_submit';
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form.
 *
 * @throws Exception
 *
 * @noinspection PhpUnused
 */
function codev_menu_form_node_form_alter(array &$form, FormStateInterface $form_state): void {
  if (!empty($form['menu']['link'])) {
    /** @var NodeForm $form_object */
    $form_object = $form_state->getFormObject();
    /** @var Node $node */
    $node = $form_object->getEntity();
    $menu_link_def = menu_ui_get_menu_link_defaults($node);
    $opts = MenuManager::getMenuItemOptionsFromTree($menu_link_def['id'] ?? '');
    _codev_menu_form_menu_link_fields($form['menu']['link'], Utils::getArrayValue('codev_menu', $opts, []));
    _codev_menu_form_menu_link_font_awesome($form['menu']['link'], $opts);
    /** @noinspection SpellCheckingInspection */
    $form['#attached']['library'][] = 'fontawesome_menu_icons/fontawesome-iconpicker';
    $form['#attached']['library'][] = 'codev_menu/node-menu-icons';
    $form['actions']['submit']['#submit'][] = '_codev_menu_form_node_form_submit';
  }
}

/**
 * Create menu link form fields.
 *
 * @noinspection PhpUnused
 */
function _codev_menu_form_menu_link_field_builder(array &$form, FormStateInterface $form_state): void {
  if (method_exists($form_state->getFormObject(), 'getEntity')) {
    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $menu_link = $form_state->getFormObject()->getEntity() ?: [];
    $link_options = $menu_link->link->first()->options ?: [];
  }
  else {
    $link_options = $form_state->getBuildInfo()['args'][0]->getOptions();
  }

  $link_options = Utils::getArrayValue('codev_menu', $link_options, []);

  $form['info']['#weight'] = -99;
  $form['path']['#weight'] = -98;

  // include custom css class field
  $form['display_settings'] = [
    '#type'       => 'container',
    '#title'      => t('Display settings'),
    '#group'      => 'sidebar',
    '#weight'     => -97,
    '#attributes' => [
      'class' => [
        'entity-meta__header',
      ],
    ],
  ];

  // move drupal enable and expanded to display settings container
  $form['display_settings']['enabled'] = $form['enabled'];
  $form['display_settings']['expanded'] = $form['expanded'];
  unset($form['enabled'], $form['expanded']);

  _codev_menu_form_menu_link_fields($form, $link_options);
}

/**
 * Return the form field as array by giving current options.
 *
 * @param array $form
 * @param array $options
 */
function _codev_menu_form_menu_link_fields(array &$form, array $options): void {
  // include custom display settings
  $visibility_options = Settings::getCategoriesAsList();
  if (!empty($visibility_options)) {
    $visibility_default = Utils::getArrayValue('hide_on', $options, []);
    $form['display_settings']['hide_on'] = [
      '#type'          => 'checkboxes',
      '#title'         => t('Do not show link on:'),
      '#description'   => t('Sets the visibility of the menu link based on the menu categories.'),
      '#options'       => $visibility_options,
      '#default_value' => $visibility_default,
    ];
  }

  // include custom link options
  $form['link_options'] = [
    '#type'   => 'details',
    '#title'  => t('Additional link options'),
    '#group'  => 'sidebar',
    '#weight' => 50,
  ];
  $form['link_options']['custom_css'] = [
    '#type'          => 'textfield',
    '#title'         => t('CSS Classes'),
    '#description'   => t('Add extended css classes to the link item'),
    '#default_value' => Utils::getArrayValue('custom_css', $options, ''),
    '#weight'        => -50,
  ];
  $target_options = Settings::getTargetsAsList();
  if (!empty($target_options)) {
    $form['link_options']['target'] = [
      '#type'          => 'select',
      '#title'         => t('Target'),
      '#description'   => t('Set HTML target attribute'),
      '#options'       => $target_options,
      '#default_value' => Utils::getArrayValue('target', $options, '_self'),
      '#weight'        => 0,
    ];
  }
}

/**
 * Font awesome icon picker field for node forms.
 *
 * @param array $form
 * @param array $options
 *
 */
function _codev_menu_form_menu_link_font_awesome(array &$form, array $options): void {
  $form['fa_icon'] = [
    '#type'   => 'details',
    '#title'  => t('FontAwesome Icon'),
    '#weight' => 99,
  ];

  /** @noinspection SpellCheckingInspection */
  $form['fa_icon']['fa_icon'] = [
    '#type'          => 'textfield',
    '#title'         => t('Icon'),
    '#default_value' => Utils::getArrayValue('fa_icon', $options, ''),
    '#attributes'    => [
      'class' => [
        'fa-menu-iconpicker',
      ],
    ],
    '#description'   => t('Note: If you are using Font Awesome 6.x, prefix the name of your icons with \'fa-\'.'),
  ];

  /** @noinspection DuplicatedCode */
  $form['fa_icon']['fa_icon_prefix'] = [
    '#type'          => 'select',
    '#title'         => t('Style prefix'),
    '#default_value' => Utils::getArrayValue('fa_icon_prefix', $options, 'fa'),
    '#options'       => [
      'fa'         => 'fa (' . t('4.x only') . ')',
      'fas'        => 'fas (' . t('5.x only') . ')',
      'far'        => 'far (' . t('5.x only') . ')',
      'fal'        => 'fal (' . t('5.x only') . ')',
      'fad'        => 'fad (' . t('5.x only') . ')',
      'fab'        => 'fab (' . t('5.x only') . ')',
      'fa-solid'   => 'Solid (' . t('6.x only') . ')',
      'fa-regular' => 'Regular (' . t('6.x only') . ')',
      'fa-light'   => 'Light (' . t('6.x only') . ')',
      'fa-thin'    => 'Thin (' . t('6.x only') . ')',
      'fa-duotone' => 'Duotone (' . t('6.x only') . ')',
      'fa-brands'  => 'Brands (' . t('6.x only') . ')',
    ],
  ];

  $form['fa_icon']['fa_icon_tag'] = [
    '#type'          => 'select',
    '#title'         => t('HTML tag'),
    '#default_value' => Utils::getArrayValue('fa_icon_tag', $options, 'i'),
    '#options'       => [
      'i'    => 'i',
      'span' => 'span',
    ],
  ];

  $form['fa_icon']['fa_icon_appearance'] = [
    '#type'          => 'select',
    '#title'         => t('Appearance'),
    '#default_value' => Utils::getArrayValue('fa_icon_appearance', $options, 'before'),
    '#options'       => [
      'before' => t('Before text'),
      'after'  => t('After text'),
      'only'   => t('Without text'),
    ],
  ];
}

/**
 * Process the submitted form for visibility handling.
 *
 * @throws EntityStorageException
 * @throws MissingDataException
 * @throws ReadOnlyException
 * @throws Exception
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_menu_form_menu_link_submit(array $form, FormStateInterface $form_state): void {
  $options = [
    'codev_menu' => _codev_menu_form_values_preformat(
      $form_state->getValue('hide_on'),
      $form_state->getValue('custom_css'),
      $form_state->getValue('target')
    ),
  ];

  if (method_exists($form_state->getFormObject(), 'getEntity')) {
    /** @var MenuLinkContent $menu_link */
    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $menu_link = $form_state->getFormObject()->getEntity();
    MenuManager::appendMenuItemOptionsToLinkContent($menu_link, $options);
  }
  else {
    $plugin_id = $form_state->getBuildInfo()['args'][0]->getPluginId();
    MenuManager::appendMenuItemOptionsToTree($plugin_id, $options);
  }
}

/**
 * Process the submitted form for visibility handling.
 *
 * @throws EntityStorageException
 * @throws MissingDataException
 * @throws ReadOnlyException
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_menu_form_node_form_submit(array $form, FormStateInterface $form_state): void {
  /** @var NodeForm $form_object */
  $form_object = $form_state->getFormObject();
  /** @var Node $node */
  $node = $form_object->getEntity();
  if (!$node->get('menu_link')->isEmpty()) {
    $mid = $node->get('menu_link')->getValue()[0]['target_id'];
    $menu = $form_state->getValue('menu');
    $menu_options = $menu['fa_icon'] ?? [];
    $menu_options['codev_menu'] = _codev_menu_form_values_preformat(
      $menu['display_settings']['hide_on'] ?? [],
      $menu['link_options']['custom_css'] ?? '',
      $menu['link_options']['target'] ?? ''
    );

    $menu_link = MenuLinkContent::load($mid);
    MenuManager::saveMenuItemOptionsToLinkContent($menu_link, $menu_options);
  }
}

/**
 * Preformats values for saving.
 *
 * Prepares and sanitizes menu link form values before saving them to the
 * database.
 *
 * @param array  $hide_on
 * @param string $custom_css
 * @param string $target
 *
 * @return array Formatted values.
 */
function _codev_menu_form_values_preformat(array $hide_on, string $custom_css, string $target): array {
  $ret = [];
  if (!empty($hide_on)) {
    $hide_on = array_filter($hide_on, function ($value) {
      return !empty($value);
    });
    $ret['hide_on'] = array_values($hide_on);
  }
  $ret['custom_css'] = Html::escape($custom_css);
  $ret['target'] = $target;
  return $ret;
}

/**
 * Add default extended menu add infos tho a form array.
 */
function _codev_menu_form_menu_defaults(array &$form, FormStateInterface $form_state): void {
  $user = Drupal::currentUser();
  if ($user->hasPermission(MenuManager::MENU_PERM_KEY)) {
    /** @var MenuForm $form_object */
    $form_object = $form_state->getFormObject();
    $form_mid = $form_object->getEntity()->id();

    // Custom role changer checkbox
    $form['permissions_cn'] = [
      '#type'        => 'details',
      '#title'       => t('Menu permissions'),
      '#description' => t('Select which user role can add or delete menu links.'),
    ];

    $form['permissions_cn']['permissions'] = [
      '#type'          => 'checkboxes',
      '#options'       => RoleManager::getCurrentUserSelectList(),
      '#default_value' => RoleManager::getMenuItemPermitted($form_mid ?: ''),
    ];

    $form['node_cn'] = [
      '#type'        => 'details',
      '#title'       => t('Node types'),
      '#description' => t('Node types that are allowed to use this menu.'),
    ];

    $form['node_cn']['nodes'] = [
      '#type'          => 'checkboxes',
      '#options'       => Settings::getNodeList(),
      '#default_value' => MenuManager::getNodeTypes($form_mid ?: ''),
    ];

    $form['actions']['submit']['#submit'][] = '_codev_menu_form_menu_update_permissions_submit';
    $form['actions']['submit']['#submit'][] = '_codev_menu_form_node_type_update_submit';
  }
  else {
    $form['third_party_settings']['menu_trail_by_path']['trail_source']['#access'] = FALSE;
  }
}

/**
 * Add default extended menu add infos tho a form array.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_menu_form_menu_update_permissions_submit(array &$form, FormStateInterface $form_state): void {
  $id = $form_state->getValue('id');
  $permissions = $form_state->getValue('permissions');
  $accept_roles = User::getAllRoles(TRUE);
  /** @var Role $highest */
  $highest = end($accept_roles);
  $permissions[$highest->id()] = $highest->id();
  foreach ($permissions as $role_id => $checked) {
    if (!empty($checked) && in_array($role_id, array_keys($accept_roles))) {
      RoleManager::addMenuItemPermission($id, $role_id);
    }
    else {
      RoleManager::removeMenuItemPermission($id, $role_id);
    }
  }
}

/**
 * Update node types allowed menu.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_menu_form_node_type_update_submit(array &$form, FormStateInterface $form_state): void {
  $mid = $form_state->getValue('id');
  $nodes = $form_state->getValue('nodes');
  try {
    /** @var NodeType[] $node_types */
    $node_types = Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($node_types as $node_type_id => $node_type) {
      $menus = $node_type->getThirdPartySetting('menu_ui', 'available_menus') ?: [];
      if (!empty($nodes[$node_type_id])) {
        if (!in_array($mid, $menus)) {
          $menus[] = $mid;
        }
      }
      else {
        if (in_array($mid, $menus)) {
          unset($menus[array_search($mid, $menus)]);
        }
      }
      $node_type->setThirdPartySetting('menu_ui', 'available_menus', $menus);
      $node_type->save();
    }
  } catch (Exception) {
  }
}
