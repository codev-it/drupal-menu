<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains kernel tests for the MenuManager utility class in the codev_menu
 * module of Drupal.
 *
 * This file provides a kernel test class for testing the MenuManager utility
 * class in the codev_menu module. It tests various functionalities provided by
 * the MenuManager, including loading menus by user, managing menu item
 * options, and appending options to menu link content.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Kernel;

use Drupal;
use Drupal\codev_menu\MenuManager;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\KernelTests\KernelTestBase;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Exception;

/**
 * Unit tests for the MenuManager utility class in the codev_menu module.
 *
 * This test class performs kernel testing of the MenuManager utility class,
 * which provides methods for managing menu items and permissions. It includes
 * tests for loading menus by user, managing menu item options in trees and
 * link content, and appending options to menu items.
 */
class MenuManagerTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'codev_menu',
    'menu_link_content',
    'link',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('menu_link_content');

    Drupal::service('router.builder')->rebuild();
  }

  /**
   * Tests MenuManager::loadMultipleByUser()
   *
   * @throws EntityStorageException
   */
  public function testLoadMultipleByUser() {
    // Empty menus without permission
    $menus = MenuManager::loadMultipleByUser();
    $this->assertEquals([], $menus);

    // Check with permissions
    $account = $this->createUser([], NULL, TRUE);
    $this->setCurrentUser($account);
    $menus = MenuManager::loadMultipleByUser($account);
    $expected = ['account', 'admin', 'footer', 'main', 'tools'];
    $this->assertEquals($expected, array_keys($menus));
  }

  /**
   * Tests MenuManager::getSelectList()
   *
   * @throws EntityStorageException
   */
  public function testGetSelectList() {
    $this->assertEquals([], MenuManager::getSelectList());

    $this->setCurrentUser($this->createUser([], NULL, TRUE));
    $this->assertEquals([
      'account' => 'User account menu',
      'admin'   => 'Administration',
      'footer'  => 'Footer',
      'main'    => 'Main navigation',
      'tools'   => 'Tools',
    ], MenuManager::getSelectList());
  }

  /**
   * Tests MenuManager::getMenuItemOptionsFromTree()
   *
   * @throws Exception
   */
  public function testGetMenuItemOptionsFromTree() {
    $test_opts = ['key' => 'value'];
    $this->setMenuTreeOptions($test_opts);
    $options = MenuManager::getMenuItemOptionsFromTree('system.admin');
    $this->assertEquals($options, $test_opts);
  }

  /**
   * Tests MenuManager::saveMenuItemOptionsToTree()
   *
   * @throws Exception
   */
  public function testSaveMenuItemOptionsToTree() {
    $test_opts = ['key' => 'value'];
    MenuManager::saveMenuItemOptionsToTree('system.admin', $test_opts);
    $this->assertEquals($this->getMenuTreeOptions(), $test_opts);
  }

  /**
   * Tests MenuManager::appendMenuItemOptionsToTree()
   *
   * @throws Exception
   */
  public function testAppendMenuItemOptionsToTree() {
    $base_opts = ['key' => 'value'];
    $test_opts = ['append_key' => 'append_value'];
    $this->setMenuTreeOptions($base_opts);
    MenuManager::appendMenuItemOptionsToTree('system.admin', $test_opts);

    $tree_opts = $this->getMenuTreeOptions();
    $accept = $base_opts + $test_opts;
    $this->assertEquals($tree_opts, $accept);
  }

  /**
   * Tests MenuManager::saveMenuItemOptionsToLinkContent()
   *
   * @throws Exception
   */
  public function testSaveMenuItemOptionsToLinkContent() {
    $menu_link = MenuLinkContent::create([
      'title'     => 'Menu link test',
      'provider'  => 'menu_link_content',
      'menu_name' => 'main',
      'link'      => ['uri' => 'internal:/'],
    ]);
    $menu_link->save();

    $this->assertEquals([], $this->getMenuContentOptions($menu_link));

    $options = ['key' => 'val'];
    MenuManager::saveMenuItemOptionsToLinkContent($menu_link, $options);

    $this->assertEquals($options, $this->getMenuContentOptions($menu_link));
  }

  /**
   * Tests MenuManager::appendMenuItemOptionsToLinkContent()
   *
   * @throws Exception
   */
  public function testAppendMenuItemOptionsToLinkContent() {
    $base_options = ['key' => 'val'];
    $menu_link = MenuLinkContent::create([
      'title'     => 'Menu link test',
      'provider'  => 'menu_link_content',
      'menu_name' => 'main',
      'link'      => [
        'uri'     => 'internal:/',
        'options' => $base_options,
      ],
    ]);
    $menu_link->save();

    $this->assertEquals($base_options, $this->getMenuContentOptions($menu_link));

    $options = ['new_key' => 'new_val'];
    MenuManager::appendMenuItemOptionsToLinkContent($menu_link, $options);

    $accept = $base_options + $options;
    $this->assertEquals($accept, $this->getMenuContentOptions($menu_link));
  }

  /**
   * Get menu tree options
   *
   * @return array
   *
   * @throws Exception
   */
  private function getMenuTreeOptions(): array {
    $connection = Drupal::database();
    $update = $connection->select('menu_tree', 'm');
    $update->fields('m', ['options']);
    $update->condition('id', 'system.admin');
    $result = $update->execute()->fetchAll();
    return unserialize($result[0]->options ?? '') ?: [];
  }

  /**
   * Set menu tree options
   *
   * @param $val
   *
   * @return void
   *
   * @throws Exception
   */
  private function setMenuTreeOptions($val): void {
    $connection = Drupal::database();
    $update = $connection->update('menu_tree');
    $update->fields(['options' => serialize($val)])
      ->condition('id', 'system.admin')
      ->execute();
  }

  /**
   * Get menu content options
   *
   * @param MenuLinkContent $menu_link
   *
   * @return array
   *
   * @throws MissingDataException
   */
  private function getMenuContentOptions(MenuLinkContent $menu_link): array {
    $menu_link_item = $menu_link->get('link')->first();
    return $menu_link_item->getValue()['options'] ?: [];
  }

}
