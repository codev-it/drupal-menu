<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains kernel tests for the RoleManager utility class in the codev_menu
 * module of Drupal.
 *
 * This file provides a kernel test class for testing the RoleManager utility
 * class in the codev_menu module. It tests the functionality of the
 * RoleManager, including retrieving the current user's roles as a select list.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Kernel;

use Drupal\codev_menu\RoleManager;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Exception;

/**
 * Unit tests for the RoleManager utility class in the codev_menu module.
 *
 * This test class performs kernel testing of the RoleManager utility class,
 * which provides methods for managing roles and their permissions related to
 * menu items. It includes tests for retrieving the current user's roles as a
 * select list.
 *
 * @noinspection PhpUnused
 */
class RoleManagerTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'codev_menu',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   *
   * @throws Exception
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');
  }

  /**
   * Tests RoleManager::getCurrentUserSelectList().
   *
   * This method tests the functionality of the
   * RoleManager::getCurrentUserSelectList() method, ensuring that it correctly
   * retrieves the current user's roles as an array suitable for select list
   * options.
   *
   * @throws EntityStorageException
   */
  public function testGetCurrentUserSelectList() {
    $this->assertEquals([], RoleManager::getCurrentUserSelectList());

    $this->setCurrentUser($this->createUser());
    $this->assertEquals([
      'anonymous' => 'Anonymous user',
    ], RoleManager::getCurrentUserSelectList());

    $this->setCurrentUser($this->createUser([], NULL, TRUE));
    $this->assertEquals([
      'anonymous'     => 'Anonymous user',
      'authenticated' => 'Authenticated user',
    ], RoleManager::getCurrentUserSelectList());
  }

}
