<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Provides kernel tests for the settings functionality in the Codev Menu
 * module of Drupal.
 *
 * This file provides a kernel test class for testing the settings
 * functionality
 * provided by the Codev Menu module. It tests the Settings utility class,
 * including methods for setting and getting configuration settings, and
 * retrieving settings as option lists.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 *
 * Provides settings tests for the Codev Menu module.
 */

namespace Drupal\Tests\codev_menu\Kernel;

use Drupal\codev_menu\Settings;
use Drupal\KernelTests\KernelTestBase;

/**
 * Unit tests for the config settings utility class in the Codev Menu module.
 *
 * This test class performs kernel testing of the Settings utility class,
 * which provides methods for managing settings in the Codev Menu module. It
 * includes tests for setting and getting configuration values, and retrieving
 * settings as option lists.
 */
class SettingsTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'codev_menu',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['codev_menu']);
  }

  /**
   * Tests Settings::set() for setting configuration values.
   */
  public function testSet() {
    $this->assertTrue(Settings::set('global_sorting', TRUE));

    $categories = ['test' => ['label' => 'test']];
    $this->assertTrue(Settings::set('categories', $categories));
  }

  /**
   * Tests Settings::get() for retrieving configuration values.
   */
  public function testGet() {
    $this->assertFalse(Settings::get('global_sorting'));

    $categories = [
      'desktop' => ['label' => t('Desktop')],
      'mobile'  => ['label' => t('Mobile')],
    ];
    $this->assertEquals(Settings::get('categories'), $categories);
  }

  /**
   * Tests Settings::getCategoriesAsList() for retrieving category options.
   */
  public function testGetCategoriesAsList() {
    $options = Settings::getCategoriesAsList();
    $expected = [
      'desktop' => t('Desktop'),
      'mobile'  => t('Mobile'),
    ];
    $this->assertEquals($expected, $options);
  }

  /**
   * Tests Settings::getTargetsAsList() for retrieving target options.
   */
  public function testGetTargetsAsList() {
    $options = Settings::getTargetsAsList();
    $expected = [
      '_blank'  => t('Opens the linked document in a new window or tab'),
      '_self'   => t('Opens the linked document in the same frame as it was clicked'),
      '_parent' => t('Opens the linked document in the parent frame'),
      '_top'    => t('Opens the linked document in the full body of the window'),
    ];
    $this->assertEquals($expected, $options);
  }

}
