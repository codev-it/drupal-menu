<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains functional tests for the admin settings form of the codev_menu
 * module in Drupal.
 *
 * This file provides a functional test class for testing the administration
 * settings form of the codev_menu module. It tests various aspects of the
 * form, including access control, form elements, and the form submission
 * process.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests the functionality of the admin settings form in the codev_menu module.
 *
 * This test class performs functional testing of the admin settings form
 * provided by the codev_menu module. It checks for access permissions, form
 * element existence, and the ability to submit the form.
 */
class AdminSettingsFormTest extends FunctionalTestBase {

  /**
   * The admin user used for testing.
   *
   * @var User|UserInterface|false
   *   A user entity with administrative permissions for testing.
   */
  protected User|UserInterface|false $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer extended menu',
    ]);
  }

  /**
   * Tests the admin settings form for functionality and access control.
   *
   * Performs various checks on the admin settings form to ensure that it is
   * functioning correctly, including access control, presence of form elements,
   * and form submission.
   *
   * @throws ExpectationException
   */
  public function testAdminSettingsForm() {
    $assert = $this->assertSession();

    // No anonymous access
    $this->drupalGet('admin/config/content/codev_menu');
    $assert->statusCodeEquals(403);

    // Admin access
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/codev_menu');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Menu link visibility categories'));
    $assert->pageTextContains(t('Label'));
    $assert->pageTextContains(t('ID'));
    $assert->buttonExists(t('Add new'));
    $assert->buttonExists(t('Delete'));
    $assert->pageTextContains(t('Sorting Settings'));
    $assert->pageTextContains(t('Activate or deactivate the global menu link sorting for mobile menu blocks.'));
    $assert->pageTextContains(t('Sorting Settings'));
    $assert->pageTextContains(t('Menu sorting:'));
    $assert->pageTextContains(t('Title'));
    $assert->pageTextContains(t('Description'));
    $assert->pageTextContains(t('Administration'));
    $assert->pageTextContains(t('Main navigation'));
    $this->submitForm([], t('Save configuration'));
  }

}
