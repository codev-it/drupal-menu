<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains functional tests for the extended menu block in the codev_menu
 * module of Drupal.
 *
 * This file provides a functional test class for testing the extended menu
 * block provided by the codev_menu module. It tests the block's form,
 * functionality, and access permissions for different user roles, including
 * admin, builder, and presenter users, as well as anonymous users.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Functional;

use Behat\Mink\Exception\ExpectationException;
use Behat\Mink\Exception\ResponseTextException;
use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests the functionality and access control of the extended menu block.
 *
 * This test class performs functional testing of the extended menu block in
 * the codev_menu module. It includes tests for block creation, access
 * permissions, form fields visibility, and the functionality of menu link
 * visibility settings.
 *
 * @noinspection PhpUnused
 */
class ExtendedMenuBlockTest extends FunctionalTestBase {

  /**
   * The admin user with permissions to administer menus and blocks.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * The builder user with permissions to administer menus and blocks.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $builderUser;

  /**
   * The presenter user with permissions to administer blocks.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $presenterUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer blocks',
      'administer menu',
      'administer menu admin',
    ]);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->builderUser = $this->drupalCreateUser([
      'administer blocks',
      'administer menu',
      'administer main menu items',
    ]);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->presenterUser = $this->drupalCreateUser([
      'administer blocks',
    ]);
  }

  /**
   * Tests the block form and functionality for different user roles.
   *
   * This method tests the block form access, field visibility, and
   * functionality for admin, builder, and presenter users, as well as
   * anonymous access. It includes tests for creating the block, verifying form
   * fields, and checking the visibility of menu links.
   *
   * @throws ExpectationException
   */
  public function testBlockForm() {
    $assert = $this->assertSession();

    // Admin access
    $this->drupalLogin($this->adminUser);

    // Check block form and field exist.
    $this->drupalGet('admin/structure/block/add/extended_menu');
    $assert->statusCodeEquals(200);
    $this->assertBlockRequiredFieldAdmin();
    $this->submitForm([], t('Save'));

    // Create combined menu block.
    $this->drupalPlaceBlock('extended_menu', [
      'id'         => 'extended_menu_admin',
      'label'      => t('Extended menu admin'),
      'menus'      => ['main'],
      'categories' => ['desktop'],
    ]);

    $this->drupalGet('admin/structure/block/manage/extended_menu_admin');
    $assert->statusCodeEquals(200);
    $this->assertBlockRequiredFieldAdmin();
    $this->submitForm([], t('Save'));

    // Builder access
    $this->drupalLogin($this->builderUser);

    // Check block form and field exist.
    $this->drupalGet('admin/structure/block/add/extended_menu');
    $assert->statusCodeEquals(200);
    $this->assertBlockRequiredFieldDefault();
    $this->submitForm([], t('Save'));

    // Create combined menu block.
    $this->drupalPlaceBlock('extended_menu', [
      'id'         => 'extended_menu',
      'label'      => t('Extended menu'),
      'menus'      => ['main'],
      'categories' => ['desktop'],
    ]);

    $this->drupalGet('admin/structure/block/manage/extended_menu');
    $assert->statusCodeEquals(200);
    $this->assertBlockRequiredFieldDefault();
    $this->submitForm([], t('Save'));

    // Presenter access
    $this->drupalLogin($this->presenterUser);

    // Check block form and field exist.
    $this->drupalGet('admin/structure/block/add/extended_menu');
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains(t('Select the menus to be displayed:'));
    $this->submitForm([], t('Save'));

    $this->drupalGet('admin/structure/block/manage/extended_menu');
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains(t('Select the menus to be displayed:'));

    // No anonymous access
    $this->drupalLogout();

    $this->drupalGet('admin/structure/block/add/extended_menu');
    $assert->statusCodeEquals(403);

    $this->drupalGet('admin/structure/block/manage/extended_menu');
    $assert->statusCodeEquals(403);
  }

  /**
   * Tests the visibility of menu links in the extended menu block.
   *
   * This method checks the functionality of menu link visibility settings in
   * the extended menu block, ensuring that links are displayed or hidden based
   * on the configured visibility categories.
   *
   * @throws ExpectationException
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   */
  public function testMenuLinkVisible() {
    $assert = $this->assertSession();
    $menu_link_content = Drupal::entityTypeManager()
      ->getStorage('menu_link_content');

    // Create block
    $this->drupalPlaceBlock('extended_menu', [
      'id'         => 'extended_menu',
      'label'      => t('Extended menu'),
      'menus'      => ['main'],
      'categories' => ['desktop'],
    ]);

    $menu_link_content->create([
      'title'     => 'Link Test',
      'menu_name' => 'main',
      'link'      => ['uri' => 'internal:/'],
    ])->save();

    $menu_link_content->create([
      'title'     => 'Mobile Test',
      'menu_name' => 'main',
      'link'      => [
        'uri'     => 'internal:/',
        'options' => ['codev_menu' => ['hide_on' => ['desktop']]],
      ],
    ])->save();

    $menu_link_content->create([
      'title'     => 'Desktop Test',
      'menu_name' => 'main',
      'link'      => [
        'uri'     => 'internal:/',
        'options' => ['codev_menu' => ['hide_on' => ['mobile']]],
      ],
    ])->save();

    $this->drupalGet('');

    $assert->linkExists('Link Test');
    $assert->linkExists('Desktop Test');
    $assert->linkNotExists('Menu Test');
  }

  /**
   * Check form base requirement block field
   *
   * @throws ResponseTextException
   */
  private function assertBlockRequiredField(): void {
    $assert = $this->assertSession();
    $assert->pageTextContains(t('Menu visibility categories'));
    $assert->pageTextContains(t('Desktop'));
    $assert->pageTextContains(t('Mobile'));
    $assert->pageTextContains(t('Select the menus to be displayed:'));
  }

  /**
   * Check form base requirement block field
   *
   * @throws ResponseTextException
   */
  private function assertBlockRequiredFieldDefault(): void {
    $assert = $this->assertSession();
    $this->assertBlockRequiredField();
    $assert->pageTextNotContains(t('Administration'));
    $assert->pageTextContains(t('Main navigation'));
  }

  /**
   * Check form admin requirement block field
   *
   * @throws ResponseTextException
   */
  private function assertBlockRequiredFieldAdmin(): void {
    $assert = $this->assertSession();
    $this->assertBlockRequiredField();
    $assert->pageTextContains(t('Administration'));
    $assert->pageTextContains(t('Main navigation'));
  }

}
