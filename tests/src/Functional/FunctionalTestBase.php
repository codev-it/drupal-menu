<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Provides a base class for functional testing in the codev_menu module of
 * Drupal.
 *
 * This file contains the FunctionalTestBase class, which serves as an abstract
 * base class for all functional tests in the codev_menu module. It extends
 * Drupal's BrowserTestBase class and sets default configurations for testing
 * environments, including the testing profile, default theme, and required
 * modules.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Abstract base class for functional testing in the codev_menu module.
 *
 * This class sets up a testing environment with predefined configurations for
 * functional tests in the codev_menu module. It includes a testing profile,
 * the 'stark' default theme, and the codev_menu module for testing purposes.
 *
 * @noinspection PhpUnused
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * The testing profile to use.
   *
   * @var string
   */
  protected $profile = 'testing';

  /**
   * The default theme for the test environment.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * List of modules to enable for the test.
   *
   * @var array
   */
  protected static $modules = [
    'codev_menu',
  ];

}
