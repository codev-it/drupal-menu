<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains functional tests for menu management and permissions in the
 * codev_menu module of Drupal.
 *
 * This file provides a functional test class for testing menu management and
 * permissions in the codev_menu module. It tests menu overview and menu link
 * forms, including access control and form functionality for different user
 * roles.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests menu management and permissions in the codev_menu module.
 *
 * This test class performs functional testing of menu management and
 * permissions in the codev_menu module. It includes tests for menu overview
 * access, menu link form functionality, and access permissions for admin and
 * builder users, as well as anonymous users.
 *
 * @noinspection PhpUnused
 */
class MenuTest extends FunctionalTestBase {

  /**
   * The admin user with permissions to administer menus.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * The builder user with limited access to certain menus and menu creation.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $builderUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer menu',
      'administer menu admin',
    ]);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->builderUser = $this->drupalCreateUser([
      'administer menu',
      'administer main menu items',
    ]);

    // Create combined menu block.
    $this->drupalPlaceBlock('system_menu_block', [
      'id'    => 'system_menu_block',
      'label' => t('Menu'),
    ]);
  }

  /**
   * Tests the menu overview for different user roles.
   *
   * This method tests the menu overview page access and functionality for
   * admin and builder users, including creating menus, verifying permissions,
   * and checking for menu item visibility.
   *
   * @throws ExpectationException
   */
  public function testMenuOverview() {
    $assert = $this->assertSession();

    $this->drupalGet('admin/structure/menu');
    $assert->statusCodeEquals(403);

    $this->drupalGet('admin/structure/menu/add');
    $assert->statusCodeEquals(403);

    $this->drupalGet('admin/structure/menu/manage/main');
    $assert->statusCodeEquals(403);

    // Admin access
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/menu');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Administration'));
    $assert->pageTextContains(t('Main navigation'));

    $this->drupalGet('admin/structure/menu/add');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Menu permissions'));
    $assert->pageTextContains(t('Node types'));

    $this->submitForm([
      'id'                         => 'menu-test',
      'label'                      => t('Menu'),
      'permissions[authenticated]' => 1,
    ], t('Save'));

    $this->drupalGet('admin/structure/menu/manage/menu-test');
    $assert->statusCodeEquals(200);
    $assert->checkboxChecked('permissions[authenticated]');

    $this->drupalGet('admin/structure/menu/manage/main');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Menu permissions'));
    $assert->pageTextContains(t('Node types'));

    // Builder access
    $this->drupalLogin($this->builderUser);
    $this->drupalGet('admin/structure/menu');
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains(t('Administration'));
    $assert->pageTextContains(t('Main navigation'));

    $this->drupalGet('admin/structure/menu/add');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Menu permissions'));
    $assert->pageTextContains(t('Node types'));

    $this->submitForm([
      'id'                         => 'menu-test2',
      'label'                      => t('Menu'),
      'permissions[authenticated]' => 1,
    ], t('Save'));

    $this->drupalGet('admin/structure/menu/manage/menu-test2');
    $assert->statusCodeEquals(200);
    $assert->checkboxChecked('permissions[authenticated]');

    $this->drupalGet('admin/structure/menu/manage/main');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Menu permissions'));
    $assert->pageTextContains(t('Node types'));
  }

  /**
   * Tests the menu link form functionality and access control.
   *
   * This method tests the menu link form for admin and builder users,
   * including creating and editing menu links with various options and
   * verifying access control for different user roles.
   *
   * @throws ExpectationException
   */
  public function testLinkForm() {
    $assert = $this->assertSession();

    // Admin access
    $this->drupalLogin($this->adminUser);

    // Menu link entity
    $this->drupalGet('admin/structure/menu/link/system.admin/edit');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Do not show link on:'));
    $assert->pageTextContains(t('FontAwesome Icon'));
    $assert->pageTextContains(t('CSS Classes'));
    $assert->pageTextContains(t('Target'));

    // Check if new link options are present.
    $this->drupalGet('admin/structure/menu/manage/main/add');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Do not show link on:'));
    $assert->pageTextContains(t('FontAwesome Icon'));
    $assert->pageTextContains(t('CSS Classes'));
    $assert->pageTextContains(t('Target'));

    // Create link with option.
    $this->submitForm([
      'title[0][value]' => t('Test Link 1'),
      'link[0][uri]'    => '<front>',
      'custom_css'      => 'test_custom_css',
      'target'          => '_blank',
    ], t('Save'));

    // Check if exist link options are present.
    $this->drupalGet('admin/structure/menu/item/1/edit');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Do not show link on:'));
    $assert->pageTextContains(t('FontAwesome Icon'));
    $assert->pageTextContains(t('CSS Classes'));
    $assert->pageTextContains(t('Target'));
    $assert->responseContains('value="test_custom_css"');
    $assert->responseContains('value="_blank"');

    // Builder access
    $this->drupalLogin($this->builderUser);

    // Menu link entity
    $this->drupalGet('admin/structure/menu/link/system.admin/edit');
    $assert->statusCodeEquals(403);

    // Check if new link options are present.
    $this->drupalGet('admin/structure/menu/manage/main/add');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Do not show link on:'));
    $assert->pageTextContains(t('FontAwesome Icon'));
    $assert->pageTextContains(t('CSS Classes'));
    $assert->pageTextContains(t('Target'));

    // Create link with option.
    $this->submitForm([
      'title[0][value]' => t('Test Link 2'),
      'link[0][uri]'    => '<front>',
      'custom_css'      => 'test_custom_css2',
      'target'          => '_parent',
    ], t('Save'));

    // Check if exist link options are present.
    $this->drupalGet('admin/structure/menu/item/2/edit');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Do not show link on:'));
    $assert->pageTextContains(t('FontAwesome Icon'));
    $assert->pageTextContains(t('CSS Classes'));
    $assert->pageTextContains(t('Target'));
    $assert->responseContains('value="test_custom_css2"');
    $assert->responseContains('value="_parent"');

    // Anonymous tests.
    $this->drupalLogout();

    $this->drupalGet('admin/structure/menu/add');
    $assert->statusCodeEquals(403);

    $this->drupalGet('admin/structure/menu/add');
    $assert->statusCodeEquals(403);

    $this->drupalGet('admin/structure/menu/item/1/edit');
    $assert->statusCodeEquals(403);
  }

}
