<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains basic functional tests for the codev_menu module in Drupal using
 * the standard profile.
 *
 * This file provides a basic functional test class for testing the codev_menu
 * module. It uses the standard profile and includes simple tests to verify the
 * module's functionality and compatibility with Drupal's standard
 * installation.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_menu\Functional;

use Behat\Mink\Exception\ExpectationException;

/**
 * Basic class for functional tests of the codev_menu module.
 *
 * This test class provides basic functional testing of the codev_menu module.
 * It includes tests to ensure that the module works correctly with Drupal's
 * standard profile and checks for schema compatibility and other requirements.
 *
 * @noinspection PhpUnused
 */
class StandardTest extends FunctionalTestBase {

  /**
   * The testing profile to use.
   */
  protected $profile = 'standard';

  /**
   * An array of config object names that are excluded from schema checking.
   *
   * @var string[]
   */
  protected static $configSchemaCheckerExclusions = [
    'fontawesome.settings',
    'redirect_after_login.settings',
  ];

  /**
   * Tests basic functionality using the standard profile.
   *
   * This method performs a basic test to ensure that the module functions
   * correctly when used with Drupal's standard profile. It checks the status
   * code of the home page to verify that the module does not interfere with
   * site access.
   *
   * @throws ExpectationException
   */
  public function testStandard() {
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);
  }

}
