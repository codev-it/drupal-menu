// noinspection DuplicatedCode,JSValidateTypes,JSUnresolvedReference

/**
 * @file
 * Behaviors of codev_menu module.
 */
(($, Drupal, once) => {
  const { behaviors } = Drupal;

  /**
   * Init node font awesome icon picker behavior.
   *
   * @type {{attach: function}}
   */
  behaviors.codevMenuNodeFontAwesomeIconPicker = {
    attach: (context) => {
      once('init-node-awesome-icon-picker', 'input[name="menu[fa_icon][fa_icon]"]', context)
        .forEach(function(item) {
          if ($.fn.iconpicker) {
            const $item = $(item);

            $item.iconpicker({
              placement   : 'topRight',
              hideOnSelect: true,
              templates   : {
                popover       : '<div class="iconpicker-popover popover"><div class="arrow"></div><div class="popover-title"></div><div class="popover-content"></div></div>',
                footer        : '<div class="popover-footer"></div>',
                buttons       : '<button class="iconpicker-btn iconpicker-btn-cancel btn btn-default btn-sm">' + Drupal.t('Cancel') + '</button><button class="iconpicker-btn iconpicker-btn-accept btn btn-primary btn-sm">' + Drupal.t('Accept') + '</button>',
                search        : '<input type="search" class="form-control iconpicker-search" placeholder="' + Drupal.t('Type to filter') + '" />',
                iconpicker    : '<div class="iconpicker"><div class="iconpicker-items"></div></div>',
                iconpickerItem: '<a role="button" href="#" class="iconpicker-item"><i></i></a>'
              }
            });

            $item.on('iconpickerSelected', function(event) {
              const $input = $('input[name="menu[fa_icon][fa_icon]"]');
              const $type = $('select[name="menu[fa_icon][fa_icon_prefix]"]');
              const parts = event.iconpickerValue.split(' ');

              if (parts.length > 1) {
                $type.val(parts[0]);
                $input.val(parts[1]);
              }

              return false;
            });
          }
        });
    }
  };
})(jQuery, Drupal, once);
